# FlowReCaptcha plugin for Craft CMS 3.x

A re-captcha field type for validating forms

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /flow-re-captcha

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for FlowReCaptcha.

## FlowReCaptcha Overview

-Insert text here-

## Configuring FlowReCaptcha

-Insert text here-

## Using FlowReCaptcha

-Insert text here-

## FlowReCaptcha Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Flow Communications](https://www.flowsa.com)
